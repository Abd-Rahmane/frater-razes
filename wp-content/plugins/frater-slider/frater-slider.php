<?php
/**
 * @package FRATER SLIDER
 * @version 1.1
 */

/**
 * Plugin Name: FRATER SLIDER
 * Plugin URI:
 * Description: Slider
 * Author:
 * Version: 1
 * Author URI:
 * Text Domain: Slider
 * License:
 * License URI:
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
} // Exit if accessed directly

add_action('init', 'codex_slider_init');

function codex_slider_init()
{
    $labels = array(
        'name' => _x('Slider', 'post type general name', 'your-plugin-textdomain'),
        'singular_name' => _x('Slid', 'post type singular name', 'your-plugin-textdomain'),
        'menu_name' => _x('Slider', 'admin menu', 'your-plugin-textdomain'),
        'name_admin_bar' => _x('Slid', 'add new on admin bar', 'your-plugin-textdomain'),
        'add_new' => _x('Ajouter un nouveau', 'slid', 'your-plugin-textdomain'),
        'add_new_item' => __('Nouveau Slid', 'your-plugin-textdomain'),
        'new_item' => __('Nouveau Slid', 'your-plugin-textdomain'),
        'edit_item' => __('Modifier Slid', 'your-plugin-textdomain'),
        'view_item' => __('Voir Slid', 'your-plugin-textdomain'),
        'all_items' => __('Touts les Slid', 'your-plugin-textdomain'),
        'search_items' => __('Cherche Slid', 'your-plugin-textdomain'),
        'parent_item_colon' => __('Parent Slid:', 'your-plugin-textdomain'),
        'not_found' => __('Aucun slid trouvés.', 'your-plugin-textdomain'),
        'not_found_in_trash' => __('Aucun slid trouvé dans la corbeille.', 'your-plugin-textdomain')
    );

    $args = array(
        'labels' => $labels,
        'description' => __('Description.', 'your-plugin-textdomain'),
        'public' => true,
        'publicly_queryable' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'query_var' => true,
        'rewrite' => array('slug' => 'slider'),
        'capability_type' => 'post',
        'has_archive' => true,
        'hierarchical' => true,
        'menu_position' => 6,
        'menu_icon' => 'dashicons-slides',
        'supports' => array('title',  'thumbnail')
    );

    register_post_type('slider', $args);
}

