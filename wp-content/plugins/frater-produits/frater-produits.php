<?php
/**
 * @package FRATER PRODUITS
 * @version 1.1
 */

/**
 * Plugin Name: FRATER PRODUITS
 * Plugin URI:
 * Description: Tous les produits de frater
 * Author:
 * Version: 1
 * Author URI:
 * Text Domain: Produit
 * License:
 * License URI:
 */

if (!defined('ABSPATH')) {
    exit;
} // Exit if accessed directly


// Register Custom Post Type Produit
function create_produit_cpt()
{

    $labels = array(
        'name' => _x('Produits', 'Post Type General Name', 'textdomain'),
        'singular_name' => _x('Produit', 'Post Type Singular Name', 'textdomain'),
        'menu_name' => _x('Produits', 'Admin Menu text', 'textdomain'),
        'name_admin_bar' => _x('Produit', 'Add New on Toolbar', 'textdomain'),
        'archives' => __('Produit Archives', 'textdomain'),
        'attributes' => __('Produit Attributes', 'textdomain'),
        'parent_item_colon' => __('Parent Produit:', 'textdomain'),
        'all_items' => __('All Produits', 'textdomain'),
        'add_new_item' => __('Add New Produit', 'textdomain'),
        'add_new' => __('Add New', 'textdomain'),
        'new_item' => __('New Produit', 'textdomain'),
        'edit_item' => __('Edit Produit', 'textdomain'),
        'update_item' => __('Update Produit', 'textdomain'),
        'view_item' => __('View Produit', 'textdomain'),
        'view_items' => __('View Produits', 'textdomain'),
        'search_items' => __('Search Produit', 'textdomain'),
        'not_found' => __('Not found', 'textdomain'),
        'not_found_in_trash' => __('Not found in Trash', 'textdomain'),
        'featured_image' => __('Featured Image', 'textdomain'),
        'set_featured_image' => __('Set featured image', 'textdomain'),
        'remove_featured_image' => __('Remove featured image', 'textdomain'),
        'use_featured_image' => __('Use as featured image', 'textdomain'),
        'insert_into_item' => __('Insert into Produit', 'textdomain'),
        'uploaded_to_this_item' => __('Uploaded to this Produit', 'textdomain'),
        'items_list' => __('Produits list', 'textdomain'),
        'items_list_navigation' => __('Produits list navigation', 'textdomain'),
        'filter_items_list' => __('Filter Produits list', 'textdomain'),
    );
    $args = array(
        'label' => __('Produit', 'textdomain'),
        'description' => __('Tous les produits de Dendani', 'textdomain'),
        'labels' => $labels,
        'menu_icon' => 'dashicons-admin-network',
        'supports' => array('title', 'thumbnail' ),
        'taxonomies' => array(),
        'public' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'menu_position' => 5,
        'show_in_admin_bar' => true,
        'show_in_nav_menus' => true,
        'can_export' => true,
        'has_archive' => true,
        'hierarchical' => true,
        'exclude_from_search' => false,
        'show_in_rest' => true,
        'publicly_queryable' => true,
        'capability_type' => 'post',
    );
    register_post_type('produits', $args);

}

add_action('init', 'create_produit_cpt', 0);


/****************/

// Register Taxonomy Specialite
function create_specialite_tax()
{

	$labels = array(
		'name' => _x('Specialites', 'taxonomy general name', 'textdomain'),
		'singular_name' => _x('Specialite', 'taxonomy singular name', 'textdomain'),
		'search_items' => __('Search Specialites', 'textdomain'),
		'all_items' => __('All Specialites', 'textdomain'),
		'parent_item' => __('Parent Specialite', 'textdomain'),
		'parent_item_colon' => __('Parent Specialite:', 'textdomain'),
		'edit_item' => __('Edit Specialite', 'textdomain'),
		'update_item' => __('Update Specialite', 'textdomain'),
		'add_new_item' => __('Add New Specialite', 'textdomain'),
		'new_item_name' => __('New Specialite Name', 'textdomain'),
		'menu_name' => __('Specialite', 'textdomain'),
	);
	$args = array(
		'labels' => $labels,
		'description' => __('Specialites des produits Frater', 'textdomain'),
		'hierarchical' => true,
		'public' => true,
		'publicly_queryable' => true,
		'show_ui' => true,
		'show_in_menu' => true,
		'show_in_nav_menus' => true,
		'show_tagcloud' => true,
		'show_in_quick_edit' => true,
		'show_admin_column' => true,
		'show_in_rest' => true,
	);
	register_taxonomy('specialites', array('produits'), $args);

}

add_action('init', 'create_specialite_tax');

// Register Taxonomy Forme
function create_forme_tax()
{

	$labels = array(
		'name' => _x('Formes', 'taxonomy general name', 'textdomain'),
		'singular_name' => _x('Forme', 'taxonomy singular name', 'textdomain'),
		'search_items' => __('Search Formes', 'textdomain'),
		'all_items' => __('All Formes', 'textdomain'),
		'parent_item' => __('Parent Forme', 'textdomain'),
		'parent_item_colon' => __('Parent Forme:', 'textdomain'),
		'edit_item' => __('Edit Forme', 'textdomain'),
		'update_item' => __('Update Forme', 'textdomain'),
		'add_new_item' => __('Add New Forme', 'textdomain'),
		'new_item_name' => __('New Forme Name', 'textdomain'),
		'menu_name' => __('Forme', 'textdomain'),
	);
	$args = array(
		'labels' => $labels,
		'description' => __('Formes des produits Frater', 'textdomain'),
		'hierarchical' => true,
		'public' => true,
		'publicly_queryable' => true,
		'show_ui' => true,
		'show_in_menu' => true,
		'show_in_nav_menus' => true,
		'show_tagcloud' => true,
		'show_in_quick_edit' => true,
		'show_admin_column' => true,
		'show_in_rest' => true,
	);
	register_taxonomy('formes', array('produits'), $args);

}

add_action('init', 'create_forme_tax');