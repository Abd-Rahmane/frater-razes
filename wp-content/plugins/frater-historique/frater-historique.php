<?php
/**
 * @package FRATER HISTORIQUES
 * @version 1.1
 */

/**
 * Plugin Name: FRATER HISTORIQUES
 * Plugin URI:
 * Description: Historiques
 * Author: Shift'IN
 * Version: 1
 * Author URI:
 * Text Domain: historiques
 * License:
 * License URI:
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
} // Exit if accessed directly

add_action('init', 'codex_historique_init');

function codex_historique_init()
{
    $labels = array(
        'name' => _x('Historiques', 'post type general name', 'your-plugin-textdomain'),
        'singular_name' => _x('Historique', 'post type singular name', 'your-plugin-textdomain'),
        'menu_name' => _x('Historiques', 'admin menu', 'your-plugin-textdomain'),
        'name_admin_bar' => _x('Historique', 'add new on admin bar', 'your-plugin-textdomain'),
        'add_new' => _x('Ajouter une nouvelle', 'historique', 'your-plugin-textdomain'),
        'add_new_item' => __('Nouveau Historique', 'your-plugin-textdomain'),
        'new_item' => __('Nouveau Historique', 'your-plugin-textdomain'),
        'edit_item' => __('Modifier Historique', 'your-plugin-textdomain'),
        'view_item' => __('Voir Historique', 'your-plugin-textdomain'),
        'all_items' => __('Toutes les Historiques', 'your-plugin-textdomain'),
        'search_items' => __('Cherche Historiques', 'your-plugin-textdomain'),
        'parent_item_colon' => __('Parent Historiques:', 'your-plugin-textdomain'),
        'not_found' => __('Aucun historique trouvés.', 'your-plugin-textdomain'),
        'not_found_in_trash' => __('Aucun historique trouvés dans la corbeille.', 'your-plugin-textdomain')
    );

    $args = array(
        'labels' => $labels,
        'description' => __('Description.', 'your-plugin-textdomain'),
        'public' => true,
        'publicly_queryable' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'query_var' => true,
        'rewrite' => array('slug' => 'historiques'),
        'capability_type' => 'post',
        'has_archive' => false,
        'hierarchical' => true,
        'menu_position' => 6,
        'menu_icon' => 'dashicons-smiley',
        'supports' => array('title')
    );

    register_post_type('historiques', $args);
}

