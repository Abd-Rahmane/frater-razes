<?php
/**
 * @package FRATER complements
 * @version 1.1
 */

/**
 * Plugin Name: FRATER complements
 * Plugin URI:
 * Description: Tous les complements de frater
 * Author:
 * Version: 1
 * Author URI:
 * Text Domain: complement
 * License:
 * License URI:
 */

if (!defined('ABSPATH')) {
    exit;
} // Exit if accessed directly


// Register Custom Post Type complement
function create_complement_cpt()
{

    $labels = array(
        'name' => _x('complements', 'Post Type General Name', 'textdomain'),
        'singular_name' => _x('complement', 'Post Type Singular Name', 'textdomain'),
        'menu_name' => _x('complements', 'Admin Menu text', 'textdomain'),
        'name_admin_bar' => _x('complement', 'Add New on Toolbar', 'textdomain'),
        'archives' => __('complement Archives', 'textdomain'),
        'attributes' => __('complement Attributes', 'textdomain'),
        'parent_item_colon' => __('Parent complement:', 'textdomain'),
        'all_items' => __('All complements', 'textdomain'),
        'add_new_item' => __('Add New complement', 'textdomain'),
        'add_new' => __('Add New', 'textdomain'),
        'new_item' => __('New complement', 'textdomain'),
        'edit_item' => __('Edit complement', 'textdomain'),
        'update_item' => __('Update complement', 'textdomain'),
        'view_item' => __('View complement', 'textdomain'),
        'view_items' => __('View complements', 'textdomain'),
        'search_items' => __('Search complement', 'textdomain'),
        'not_found' => __('Not found', 'textdomain'),
        'not_found_in_trash' => __('Not found in Trash', 'textdomain'),
        'featured_image' => __('Featured Image', 'textdomain'),
        'set_featured_image' => __('Set featured image', 'textdomain'),
        'remove_featured_image' => __('Remove featured image', 'textdomain'),
        'use_featured_image' => __('Use as featured image', 'textdomain'),
        'insert_into_item' => __('Insert into complement', 'textdomain'),
        'uploaded_to_this_item' => __('Uploaded to this complement', 'textdomain'),
        'items_list' => __('complements list', 'textdomain'),
        'items_list_navigation' => __('complements list navigation', 'textdomain'),
        'filter_items_list' => __('Filter complements list', 'textdomain'),
    );
    $args = array(
        'label' => __('complement', 'textdomain'),
        'description' => __('Tous les complements de Dendani', 'textdomain'),
        'labels' => $labels,
        'menu_icon' => 'dashicons-admin-network',
        'supports' => array('title', 'thumbnail' ),
        'taxonomies' => array(),
        'public' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'menu_position' => 5,
        'show_in_admin_bar' => true,
        'show_in_nav_menus' => true,
        'can_export' => true,
        'has_archive' => true,
        'hierarchical' => true,
        'exclude_from_search' => false,
        'show_in_rest' => true,
        'publicly_queryable' => true,
        'capability_type' => 'post',
    );
    register_post_type('complements', $args);

}

add_action('init', 'create_complement_cpt', 0);

// Register Taxonomy categorie
function create_complementscat_tax()
{

	$labels = array(
		'name' => _x('categories', 'taxonomy general name', 'textdomain'),
		'singular_name' => _x('categorie', 'taxonomy singular name', 'textdomain'),
		'search_items' => __('Search categories', 'textdomain'),
		'all_items' => __('All categories', 'textdomain'),
		'parent_item' => __('Parent categorie', 'textdomain'),
		'parent_item_colon' => __('Parent categorie:', 'textdomain'),
		'edit_item' => __('Edit categorie', 'textdomain'),
		'update_item' => __('Update categorie', 'textdomain'),
		'add_new_item' => __('Add New categorie', 'textdomain'),
		'new_item_name' => __('New categorie Name', 'textdomain'),
		'menu_name' => __('categorie', 'textdomain'),
	);
	$args = array(
		'labels' => $labels,
		'description' => __('categories des produits Frater', 'textdomain'),
		'hierarchical' => true,
		'public' => true,
		'publicly_queryable' => true,
		'show_ui' => true,
		'show_in_menu' => true,
		'show_in_nav_menus' => true,
		'show_tagcloud' => true,
		'show_in_quick_edit' => true,
		'show_admin_column' => true,
		'show_in_rest' => true,
	);
	register_taxonomy('complementscat', array('complements'), $args);

}

add_action('init', 'create_complementscat_tax');
