<?php
/**
 * @package FRATER PARTENAIRES
 * @version 1.1
 */

/**
 * Plugin Name: FRATER PARTENAIRES
 * Plugin URI:
 * Description: Partenaires
 * Author: Shift'IN
 * Version: 1
 * Author URI:
 * Text Domain: partenaires
 * License:
 * License URI:
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
} // Exit if accessed directly

add_action('init', 'codex_partenaire_init');

function codex_partenaire_init()
{
    $labels = array(
        'name' => _x('Partenaires', 'post type general name', 'your-plugin-textdomain'),
        'singular_name' => _x('Partenaire', 'post type singular name', 'your-plugin-textdomain'),
        'menu_name' => _x('Partenaires', 'admin menu', 'your-plugin-textdomain'),
        'name_admin_bar' => _x('Partenaire', 'add new on admin bar', 'your-plugin-textdomain'),
        'add_new' => _x('Ajouter une nouvelle', 'partenaire', 'your-plugin-textdomain'),
        'add_new_item' => __('Nouveau Partenaire', 'your-plugin-textdomain'),
        'new_item' => __('Nouveau Partenaire', 'your-plugin-textdomain'),
        'edit_item' => __('Modifier Partenaire', 'your-plugin-textdomain'),
        'view_item' => __('Voir Partenaire', 'your-plugin-textdomain'),
        'all_items' => __('Toutes les Partenaires', 'your-plugin-textdomain'),
        'search_items' => __('Cherche Partenaires', 'your-plugin-textdomain'),
        'parent_item_colon' => __('Parent Partenaires:', 'your-plugin-textdomain'),
        'not_found' => __('Aucun partenaire trouvés.', 'your-plugin-textdomain'),
        'not_found_in_trash' => __('Aucun partenaire trouvés dans la corbeille.', 'your-plugin-textdomain')
    );

    $args = array(
        'labels' => $labels,
        'description' => __('Description.', 'your-plugin-textdomain'),
        'public' => true,
        'publicly_queryable' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'query_var' => true,
        'rewrite' => array('slug' => 'partenaires'),
        'capability_type' => 'post',
        'has_archive' => false,
        'hierarchical' => true,
        'menu_position' => 6,
        'menu_icon' => 'dashicons-smiley',
        'supports' => array('title', 'editor', 'thumbnail')
    );

    register_post_type('partenaires', $args);
}

