<?php
/**
 * @package FRATER OFFRES
 * @version 1.1
 */

/**
 * Plugin Name: FRATER OFFRES
 * Plugin URI:
 * Description: Offres
 * Author: Shift'IN
 * Version: 1
 * Author URI:
 * Text Domain: offres
 * License:
 * License URI:
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
} // Exit if accessed directly

add_action('init', 'codex_offre_init');

function codex_offre_init()
{
    $labels = array(
        'name' => _x('Offres', 'post type general name', 'your-plugin-textdomain'),
        'singular_name' => _x('Offre', 'post type singular name', 'your-plugin-textdomain'),
        'menu_name' => _x('Offres', 'admin menu', 'your-plugin-textdomain'),
        'name_admin_bar' => _x('Offre', 'add new on admin bar', 'your-plugin-textdomain'),
        'add_new' => _x('Ajouter une nouvelle', 'offre', 'your-plugin-textdomain'),
        'add_new_item' => __('Nouveau Offre', 'your-plugin-textdomain'),
        'new_item' => __('Nouveau Offre', 'your-plugin-textdomain'),
        'edit_item' => __('Modifier Offre', 'your-plugin-textdomain'),
        'view_item' => __('Voir Offre', 'your-plugin-textdomain'),
        'all_items' => __('Toutes les Offres', 'your-plugin-textdomain'),
        'search_items' => __('Cherche Offres', 'your-plugin-textdomain'),
        'parent_item_colon' => __('Parent Offres:', 'your-plugin-textdomain'),
        'not_found' => __('Aucun offre trouvés.', 'your-plugin-textdomain'),
        'not_found_in_trash' => __('Aucun offre trouvés dans la corbeille.', 'your-plugin-textdomain')
    );

    $args = array(
        'labels' => $labels,
        'description' => __('Description.', 'your-plugin-textdomain'),
        'public' => true,
        'publicly_queryable' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'query_var' => true,
        'rewrite' => array('slug' => 'offres'),
        'capability_type' => 'post',
        'has_archive' => true,
        'hierarchical' => true,
        'menu_position' => 6,
        'menu_icon' => 'dashicons-smiley',
        'supports' => array('title', 'thumbnail')
    );

    register_post_type('offres', $args);
}

