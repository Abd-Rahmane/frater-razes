<?php
/**
 * @package FRATER FILIALES
 * @version 1.1
 */

/**
 * Plugin Name: FRATER FILIALES
 * Plugin URI:
 * Description: Filiales
 * Author: Shift'IN
 * Version: 1
 * Author URI:
 * Text Domain: filiales
 * License:
 * License URI:
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
} // Exit if accessed directly

add_action('init', 'codex_filiale_init');

function codex_filiale_init()
{
    $labels = array(
        'name' => _x('Filiales', 'post type general name', 'your-plugin-textdomain'),
        'singular_name' => _x('Filiale', 'post type singular name', 'your-plugin-textdomain'),
        'menu_name' => _x('Filiales', 'admin menu', 'your-plugin-textdomain'),
        'name_admin_bar' => _x('Filiale', 'add new on admin bar', 'your-plugin-textdomain'),
        'add_new' => _x('Ajouter une nouvelle', 'filiale', 'your-plugin-textdomain'),
        'add_new_item' => __('Nouveau Filiale', 'your-plugin-textdomain'),
        'new_item' => __('Nouveau Filiale', 'your-plugin-textdomain'),
        'edit_item' => __('Modifier Filiale', 'your-plugin-textdomain'),
        'view_item' => __('Voir Filiale', 'your-plugin-textdomain'),
        'all_items' => __('Toutes les Filiales', 'your-plugin-textdomain'),
        'search_items' => __('Cherche Filiales', 'your-plugin-textdomain'),
        'parent_item_colon' => __('Parent Filiales:', 'your-plugin-textdomain'),
        'not_found' => __('Aucun filiale trouvés.', 'your-plugin-textdomain'),
        'not_found_in_trash' => __('Aucun filiale trouvés dans la corbeille.', 'your-plugin-textdomain')
    );

    $args = array(
        'labels' => $labels,
        'description' => __('Description.', 'your-plugin-textdomain'),
        'public' => true,
        'publicly_queryable' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'query_var' => true,
        'rewrite' => array('slug' => 'filiales'),
        'capability_type' => 'post',
        'has_archive' => true,
        'hierarchical' => true,
        'menu_position' => 6,
        'menu_icon' => 'dashicons-smiley',
        'supports' => array('title', 'thumbnail')
    );

    register_post_type('filiales', $args);
}

