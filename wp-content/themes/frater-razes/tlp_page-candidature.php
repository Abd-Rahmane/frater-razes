<?php
/*
Template Name: Candidature Spontanées
*/
?>
<?php
get_header();
$context                = Timber::get_context();
$context['racine']      = get_template_directory_uri();
$context['site']        = esc_url( home_url( '/' ) );
$context['posts']       = Timber::get_posts(array('pagename' => 'candidatures-spontanees'));
$context['formulaires'] = get_field( 'formulaire_de_condidature' );
$context['id_form']     = $context['formulaires']->ID;
$context['formulaire']  = do_shortcode( '[contact-form-7 id="' . $context['id_form'] . '"]' );

if ( 'en' == pll_current_language() ) {
	$context['menu'] = new \Timber\Menu( 326 );
} else {
	$context['menu'] = new \Timber\Menu( 33 );
}

$context['fil']  = do_shortcode( '[wpseo_breadcrumb]' );

Timber::render('page-candidature.html.twig', $context);

get_footer();

?>