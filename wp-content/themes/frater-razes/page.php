<?php
get_header();
$context                = Timber::get_context();
$context['racine']      = get_template_directory_uri();
$context['site']        = esc_url( home_url( '/' ) );
//$context['menu'] = new \Timber\Menu( 'Le groupe' );

$timber_post = new Timber\Post();
$context['post'] = $timber_post;

$context['fil']  = do_shortcode( '[wpseo_breadcrumb]' );

Timber::render('page-default.html.twig', $context);

get_footer();

?>