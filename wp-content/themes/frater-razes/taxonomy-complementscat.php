<?php
get_header();
echo "<style>.pageHeaderTitle {  display: none;}</style>";

$produits = array(
	'post_type'      => 'complements',
	'posts_per_page' => - 1,
	'order'          => 'ASC',
	'tax_query' => array(
		array(
			'taxonomy' => 'complementscat',
			'field' => 'id',
			'terms' => get_queried_object_id(),
			'include_children' => false
		)
	)
);

$context           = Timber::get_context();
$context['racine'] = get_template_directory_uri();
$context['site']   = esc_url( home_url( '/' ) );
$context['termlink'] = 'complements';
$context['filter'] = 0;

$context['produits'] = new Timber\PostQuery($produits );

$context['specialite'] = get_term_by('id', get_queried_object_id(), 'complementscat');
//$context['image'] = get_field('image_specialite', get_queried_object());
$context['image'] = get_field('image_couverture', get_queried_object());

$context['fil']  = do_shortcode( '[wpseo_breadcrumb]' );
$context['lang'] = pll_current_language();

Timber::render( 'page-listeProduits.html.twig', $context );

get_footer();

?>