<?php
/*
Template Name: Nom Histoire
*/
?>
<?php
get_header();
$context                = Timber::get_context();
$context['racine']      = get_template_directory_uri();
$context['site']        = esc_url( home_url( '/' ) );
$context['posts']       = Timber::get_posts(array('pagename' => 'un-nom-une-histoire'));
if ( 'en' == pll_current_language() ) {
	$context['menu'] = new \Timber\Menu( 323 );
} else {
	$context['menu'] = new \Timber\Menu( 20 );
}
$context['fil']  = do_shortcode( '[wpseo_breadcrumb]' );

Timber::render('page-nomHistoire.html.twig', $context);

get_footer();

?>