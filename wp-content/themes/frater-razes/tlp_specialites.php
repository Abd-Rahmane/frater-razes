<?php
/*
Template Name: Spécialités
*/
?>
<?php
get_header();

$context           = Timber::get_context();
$context['racine'] = get_template_directory_uri();
$context['site']   = esc_url( home_url( '/' ) );

$context['posts'] = Timber::get_posts( array( 'pagename' => 'specialites' ) );
$context['specialites'] = Timber::get_terms('specialites');
$context['complements'] = Timber::get_terms('complementscat');

$context['fil']  = do_shortcode( '[wpseo_breadcrumb]' );

Timber::render( 'page-specialites.html.twig', $context );

get_footer();

?>