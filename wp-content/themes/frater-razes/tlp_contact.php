<?php
/*
Template Name: Contact
*/
?>
<?php
get_header();
$context                = Timber::get_context();
$context['racine']      = get_template_directory_uri();
$context['site']        = esc_url( home_url( '/' ) );

$context['posts']       = Timber::get_posts(array('pagename' => 'contact'));
$context['formulaires'] = get_field( 'formulaire' );
$context['social'] = get_field('reseaux_sociaux', 967);
$context['id_form']     = $context['formulaires']->ID;
$context['formulaire']  = do_shortcode( '[contact-form-7 id="' . $context['id_form'] . '"]' );
$context['fil']  = do_shortcode( '[wpseo_breadcrumb]' );
$context['lang'] = pll_current_language();
Timber::render('page-contact.html.twig', $context);

get_footer();

?>