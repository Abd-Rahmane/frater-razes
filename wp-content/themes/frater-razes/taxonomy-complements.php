<?php
get_header();

$produits = array(
	'post_type'      => 'complements',
	'posts_per_page' => - 1,
	'order'          => 'ASC',
	'tax_query' => array(
		array(
			'taxonomy' => 'complimentscat',
			'field' => 'id',
			'terms' => get_queried_object_id(),
			'include_children' => false
		)
	)
);

$context           = Timber::get_context();
$context['racine'] = get_template_directory_uri();
$context['site']   = esc_url( home_url( '/' ) );

//$context['posts'] = Timber::get_posts( array( 'pagename' => 'lhistorique-du-groupe' ) );
$context['produits'] = new Timber\PostQuery($produits );
$context['lang'] = pll_current_language();

//$context['formes'] = Timber::get_terms('formes');


Timber::render( 'page-listeProduits.html.twig', $context );

get_footer();

?>