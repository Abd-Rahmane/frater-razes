<?php
/**
 * The template for displaying search results pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#search-result
 *
 * @package opera_alger
 */

get_header();
?>

	<section class="pageHeader header-2 before-radius-40 after-radius-40">

	<div class="overlay-slider-page " ></div>
	<div class="fixed-bottom-center mb-5">
		<div class="container">
			<div class="row d-flex align-items-end mb-3">
				<div class="col">

					<h1 class="pageHeaderTitle">Recherche</h1>
				</div>

			</div>
		</div>
	</div>
	</section>
<div class="clear"><br></div>

	<div class="container mb-5">
		<div class="row">
			<h2>Résultats de recherche pour : <strong>"<?php the_search_query(); ?>"</strong></h2>
		</div>
			<div class="row">
			<?php if ( have_posts() ) : ?>
				<?php /* Start the Loop */
				while ( have_posts() ) : the_post(); ?>

						<div class="col-12" style="border-bottom: 1px solid #05344d1a">
						<a href="<?php the_permalink(); ?>" class="color-2 pl-3">
							<h5>
								<?php the_title(); ?>
							</h5>
						</a>
						</div>

				<?php endwhile;
				else :?>
			</div>
				<div class="my-4" style="text-align: center;">
					<p class="color-1" style="font-size:30px;"> Aucun article ne correspond à votre recherche</p><br>
				</div>
			<?php endif; ?>
	</div>
</div>
	<section class="home-bottom gray-bg p-3 ">
		<div class="container">
			<div class="row ">
				<div class="col-12 col-sm-12 col-md-6 col-lg-8 home-bottom-left flex-center">
					<p class="m-0">Notre <b>Savoir</b> Nourrit Votre <b>SANTÉ</b></p>
				</div>
				<div class="col-12 col-sm-12 col-md-6 col-lg-4 p-2 home-bottom-right">
					<div class="row">
						<div class="col-6 col-sm-6 col-md-6">
							<!--<div class="text-right flex-center-center"><i class="fas fa-headset m-2"></i><span class="fs-2">Service consommateur<br> +213 (0) 23 53 81 19</span>
							</div>-->
						</div>
						<div class="col-6 col-sm-6 col-md-6">
							<!--<div class="text-right flex-center-center"><i class="fas fa-globe-americas"></i>
								<div class="dropdown "><a class="btn dropdown-toggle fs-2" href="#" role="button"
														  id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true"
														  aria-expanded="false"> Langue </a>
									<div class="dropdown-menu fs-2" aria-labelledby="dropdownMenuLink"><a
												class="dropdown-item active" href="#">Fr</a> <a class="dropdown-item"
																								href="#">En</a></div>
								</div>
							</div>-->
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
<?php

get_footer();
