<?php
get_header();

$produits = array(
	'post_type'      => 'produits',
	'posts_per_page' => - 1,
	'order'          => 'ASC',
	'tax_query' => array(
		array(
			'taxonomy' => 'specialites',
			'field' => 'id',
			'terms' => get_queried_object_id(),
			'include_children' => false
		)
	)
);

$context           = Timber::get_context();
$context['racine'] = get_template_directory_uri();
$context['site']   = esc_url( home_url( '/' ) );
$context['siteURL']   = get_site_url();
$context['termlink'] = 'produits';
$context['filter'] = 1;

$context['produits'] = new Timber\PostQuery($produits );

$context['formes'] = Timber::get_terms('formes');

$context['specialite'] = get_term_by('id', get_queried_object_id(), 'specialites');
$context['image'] = get_field('image_specialite', get_queried_object());
$context['fil']  = do_shortcode( '[wpseo_breadcrumb]' );
$context['lang'] = pll_current_language();
Timber::render( 'page-listeProduits.html.twig', $context );

get_footer();

?>