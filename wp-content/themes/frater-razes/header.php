<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package frater-razes
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-157843522-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];

        function gtag() {
            dataLayer.push(arguments);
        }

        gtag('js', new Date());

        gtag('config', 'UA-157843522-1');
    </script>

    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="profile" href="https://gmpg.org/xfn/11">
    <link rel="icon" href="<?php echo get_template_directory_uri(); ?>/favicon.png">

	<?php wp_head(); ?>
	<?php $lang = 'version-' . pll_current_language(); ?>
</head>
<body <?php body_class( $lang ); ?>>
<!--top header-->
<header class="">
    <!--most top info -->
    <!--<div style="height: 40px; background: #f2f9ee; margin-right: 5%; "></div>-->
    <section class="home-top gray-bg p-0 d-none d-sm-block ">
        <div class="container">
            <div class="row ">
                <div class="col-12 col-sm-12 col-md-5 col-lg-7 home-bottom-left flex-center d-none d-sm-none d-md-block">

					<?php if ( pll_current_language() == 'en' ) { ?>
                        <p class="m-0 py-3">Our <b>Knowledge</b> Nourishes Your <b>HEALTH</b></p>
					<?php } else { ?>
                        <p class="m-0 py-3">Notre <b>Savoir</b> Nourrit Votre <b>SANTÉ</b></p>
					<?php } ?>

                </div>
                <div class="col-12 col-sm-12 col-md-7 col-lg-5 home-bottom-right">
                    <div class="row h-100">
                        <div class="col">
                            <form action="<?php echo esc_url( home_url( '/' ) ); ?>" method="post" class="w-100 h-100">
                                <div class="input-group h-100">
                                    <input type="text" class="form-control" name="s" placeholder="Rechercher"
                                           aria-label="Recipient's username" aria-describedby="basic-addon2" style="height: 100%;
background-color: #dce2e6; border-radius: 0px; border: none;">
                                    <div class="input-group-append">
                                        <button class="btn btn-outline-secondary" type="button" style="border: none;
background-color: #dce2e6;">
                                            <i class="fas fa-search"></i>
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <!--<div class="text-right flex-center-center"><i class="fas fa-headset m-2"></i><span
									class="fs-2">Service consommateur<br> +213 (0) 23 53 81 19</span></div>-->

                        <!--<div class="col-6 col-sm-5 col-md-4">-->
                        <div class="col">
                            <div class="text-right flex-center-center h-100">
                                <i class="fas fa-globe-americas"></i>
                                <div class="dropdown ">
                                    <a class="btn dropdown-toggle fs-2" href="#" role="button"
                                       id="dropdownMenuLink" data-toggle="dropdown"
                                       aria-haspopup="true" aria-expanded="false"> Langue </a>
                                    <div class="dropdown-menu fs-2" aria-labelledby="dropdownMenuLink">
                                    <?php dynamic_sidebar( 'sidebar-2' ); ?>
                                    <!--<a class="dropdown-item active" href="#">Fr</a>
                                    <a class="dropdown-item" href="#">En</a>-->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- End most top info -->

    <div class="logo-menu bg-light" data-toggle="sticky-onscroll">
        <div class="logo-div">
            <a href="<?php echo esc_url( home_url( '/' ) ); ?>">
                <div class="logo-frater"></div>
                <!--<img src="<?php echo get_template_directory_uri(); ?>/views/img/logo-blanc.svg" alt="FRATER-RAZES"
                     class="hideIsSticky img-fluid">
                <img src="<?php echo get_template_directory_uri(); ?>/views/img/logo.svg" alt="FRATER-RAZES"
                     class="showIsSticky img-fluid">-->
            </a>
        </div>
		<?php
		wp_nav_menu( array(
			'theme_location' => 'menu-1',
		) );
		?>
    </div>
</header> <!-- End top header-->
