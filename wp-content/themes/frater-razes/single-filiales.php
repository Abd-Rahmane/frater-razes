<?php
get_header();

$filiales = array(
	'post_type'      => 'filiales',
	'posts_per_page' => -1,
	'order'          => 'ASC'
);

$context           = Timber::get_context();
$context['racine'] = get_template_directory_uri();
$context['site']   = esc_url( home_url( '/' ) );

$context['filiales'] = new Timber\PostQuery( $filiales );

$context['post'] = new Timber\Post();
$context['images'] = acf_photo_gallery('gallerie_filiale', $context['post']->ID);

$context['menu'] = new \Timber\Menu( 'Nos filiales' );

$context['fil']  = do_shortcode( '[wpseo_breadcrumb]' );

Timber::render( 'page-filiales.html.twig', $context );

get_footer();

?>