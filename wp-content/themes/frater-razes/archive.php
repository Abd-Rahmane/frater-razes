<?php
//get_header();

$posts = array(
	'post_type'      => 'post',
	'posts_per_page' => - 1,
	'order'          => 'ASC'
);

$context           = Timber::get_context();
$context['racine'] = get_template_directory_uri();
$context['site']   = esc_url( home_url( '/' ) );

$context['pages'] = Timber::get_posts( array( 'pagename' => 'actualites' ) );
$context['posts'] = new Timber\PostQuery();
/*
echo '<h1>archiiiive</h1>';
die;
*/
Timber::render( 'page-listeActu.html.twig', $context );

get_footer();

?>