<?php

get_header();


$context = Timber::get_context();
$context['site'] = esc_url(home_url('/'));
$context['racine'] = get_template_directory_uri();
$context['posts'] = Timber::get_posts();
$context['post'] = new Timber\Post();
$context['images'] = acf_photo_gallery('gallerie_actualite', $context['post']->ID);

// Related posts

$categories = get_the_terms($context['post']->ID, 'category');

$category_ids = array();
foreach ($categories as $individual_category)
	$category_ids[] = $individual_category->term_id;
$relateds = array(
	'post_type' => 'post',
	'tax_query' => array(
		array(
			'taxonomy' => 'category',
			'terms' => $category_ids,
		),
	),
	'post__not_in' => array($context['post']->ID),
	'posts_per_page' => 2,
);

$context['lang'] = pll_current_language();
$context['relateds'] = Timber::get_posts($relateds);

$context['fil']  = do_shortcode( '[wpseo_breadcrumb]' );

Timber::render('page-actuSingle.html.twig', $context);

get_footer();

?>