<?php
/*
Template Name: Notre Approche
*/
?>
<?php
get_header();
$context           = Timber::get_context();
$context['racine'] = get_template_directory_uri();
$context['site']   = esc_url( home_url( '/' ) );
if ( 'en' == pll_current_language() ) {
	$context['menu'] = new \Timber\Menu( 322 );
} else {
	$context['menu'] = new \Timber\Menu( 19 );
}

$context['posts'] = Timber::get_posts( array( 'pagename' => 'notre-approche' ) );

$context['fil'] = do_shortcode( '[wpseo_breadcrumb]' );

Timber::render( 'page-notreApproche.html.twig', $context );

get_footer();

?>