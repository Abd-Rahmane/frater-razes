<?php
/*
Template Name: Notre Stratégie
*/
?>
<?php
get_header();
$context                = Timber::get_context();
$context['racine']      = get_template_directory_uri();
$context['site']        = esc_url( home_url( '/' ) );
$context['posts']       = Timber::get_posts(array('pagename' => 'notre-strategie'));
if ( 'en' == pll_current_language() ) {
	$context['menu'] = new \Timber\Menu( 322 );
} else {
	$context['menu'] = new \Timber\Menu( 19 );
}
$context['fil']  = do_shortcode( '[wpseo_breadcrumb]' );


Timber::render('page-notreStrategie.html.twig', $context);

get_footer();

?>