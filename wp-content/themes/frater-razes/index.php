<?php
get_header();

$context           = Timber::get_context();
$context['racine'] = get_template_directory_uri();
$context['site']   = esc_url( home_url( '/' ) );

$context['posts'] = Timber::get_posts( array( 'pagename' => 'actualites' ) );
$context['articles'] = new Timber\PostQuery();

$context['categories'] = Timber::get_terms('category', array('hide_empty' => true));

$context['fil']  = do_shortcode( '[wpseo_breadcrumb]' );

Timber::render( 'page-listeActu.html.twig', $context );

get_footer();

?>