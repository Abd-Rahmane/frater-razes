<?php
/*
Template Name: Offres
*/
?>
<?php
get_header();

$offres = array(
	'post_type'      => 'offres',
	'posts_per_page' => -1,
	'order'          => 'ASC'
);

$context           = Timber::get_context();
$context['racine'] = get_template_directory_uri();
$context['site']   = esc_url( home_url( '/' ) );

$context['posts'] = Timber::get_posts( array( 'pagename' => 'postes-a-pourvoir' ) );
$context['offres'] = new Timber\PostQuery( $offres );

if ( 'en' == pll_current_language() ) {
	$context['menu'] = new \Timber\Menu( 326 );
} else {
	$context['menu'] = new \Timber\Menu( 33 );
}

$context['fil']  = do_shortcode( '[wpseo_breadcrumb]' );

Timber::render( 'page-postePouvoir.html.twig', $context );

get_footer();

?>