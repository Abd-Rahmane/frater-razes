<?php

get_header();

$context = Timber::get_context();
$context['site'] = esc_url(home_url('/'));
$context['racine'] = get_template_directory_uri();
$context['termlink'] = 'complements';

$context['post'] = new Timber\Post();


// Related posts

$categories = get_the_terms($context['post']->ID, 'complementscat');

$context['specialite'] = $categories[0];
//$context['image'] = get_field('image_specialite', $context['specialite']);
$context['image'] = get_field('image_couverture', $context['specialite']);

$category_ids = array();
foreach ($categories as $individual_category)
	$category_ids[] = $individual_category->term_id;
$relateds = array(
	'post_type' => 'complements',
	'tax_query' => array(
		array(
			'taxonomy' => 'complementscat',
			'terms' => $category_ids,
		),
	),
	'post__not_in' => array($context['post']->ID),
	'posts_per_page' => 3,
);

$context['relateds'] = Timber::get_posts($relateds);

$context['fil']  = do_shortcode( '[wpseo_breadcrumb]' );
$context['lang'] = pll_current_language();

Timber::render('page-produitSingle.html.twig', $context);

get_footer();

?>