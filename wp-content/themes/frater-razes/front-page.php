<?php
get_header();

$slider = array(
	'post_type'      => 'slider',
	'posts_per_page' => - 1,
	'order'          => 'ASC',

);

$filiales = array(
	'post_type'      => 'filiales',
	'posts_per_page' => 5,
	'order'          => 'ASC'
);

$offres   = array(
	'post_type'      => 'offres',
	'posts_per_page' => 3,
	'order'          => 'ASC'
);

$context                = Timber::get_context();
$context['racine']      = get_template_directory_uri();
$context['site']        = esc_url( home_url( '/' ) );
$context['slider']      = Timber::get_posts( $slider );
$context['filiales']      = Timber::get_posts( $filiales );
$context['offres']      = Timber::get_posts( $offres );
$context['propos']       = Timber::get_posts( array( 'pagename' => 'qui-sommes-nous' ) );
$timber_post = new Timber\Post();
$context['post'] = $timber_post;
$context['spec']       = Timber::get_posts( array( 'pagename' => 'specialites' ) );
$context['realisations']       = Timber::get_posts( array( 'pagename' => 'realisations' ) );
$context['notreCultureRH']       = Timber::get_posts( array( 'pagename' => 'notre-culture-rh' ) );
$context['specialites'] = Timber::get_terms('specialites', array(
	'number' => 8,
) );


Timber::render( 'index.html.twig', $context );

get_footer();

?>