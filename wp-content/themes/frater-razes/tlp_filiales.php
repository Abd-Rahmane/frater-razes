<?php
/*
Template Name: Filiales
*/
?>
<?php
get_header();

$filiales = array(
	'post_type'      => 'filiales',
	'posts_per_page' => -1,
	'order'          => 'ASC'
);

$context           = Timber::get_context();
$context['racine'] = get_template_directory_uri();
$context['site']   = esc_url( home_url( '/' ) );
$context['posts'] = Timber::get_posts( array( 'pagename' => 'nos-filiales' ) );
$context['filiales'] = new Timber\PostQuery( $filiales );
if ( 'en' == pll_current_language() ) {
	$context['menu'] = new \Timber\Menu( 325 );
} else {
	$context['menu'] = new \Timber\Menu( 21 );
}
$context['fil']  = do_shortcode( '[wpseo_breadcrumb]' );

Timber::render( 'page-filiales-archive.html.twig', $context );

get_footer();

?>