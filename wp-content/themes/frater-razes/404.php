<?php
get_header();
$context                = Timber::get_context();
$context['racine']      = get_template_directory_uri();
$context['site']        = esc_url( home_url( '/' ) );
$context['posts']       = Timber::get_posts(array('pagename' => '404-2'));

if ( function_exists('yoast_breadcrumb') ) {
	$context['breadcrumbs'] = yoast_breadcrumb( '<p id="breadcrumbs">','</p>' );
};


Timber::render('page-404.html.twig', $context);

get_footer();

?>