<?php
/*
Template Name: RSE
*/
?>
<?php
get_header();
$context                = Timber::get_context();
$context['racine']      = get_template_directory_uri();
$context['site']        = esc_url( home_url( '/' ) );
$context['posts']       = Timber::get_posts(array('pagename' => 'responsabilite-sociale-de-lentreprise'));
if ( 'en' == pll_current_language() ) {
	$context['menu'] = new \Timber\Menu( 322 );
} else {
	$context['menu'] = new \Timber\Menu( 19 );
}
/*
 if ( function_exists('yoast_breadcrumb') ) {
	 $context['breadcrumbs'] = yoast_breadcrumb( '<p id="breadcrumbs">','</p>' );
};
*/
$context['fil']  = do_shortcode( '[wpseo_breadcrumb]' );
Timber::render('page-rse.html.twig', $context);

get_footer();

?>