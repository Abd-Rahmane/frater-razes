<?php
get_header();

$historiques = array(
	'post_type'      => 'historiques',
	'posts_per_page' => - 1,
	'order'          => 'ASC'
);

$context           = Timber::get_context();
$context['racine'] = get_template_directory_uri();
$context['site']   = esc_url( home_url( '/' ) );

$context['posts'] = Timber::get_posts( array( 'pagename' => 'lhistorique-du-groupe' ) );
$context['historiques'] = new Timber\PostQuery( $historiques );

$context['fil']  = do_shortcode( '[wpseo_breadcrumb]' );

Timber::render( 'page-historiques.html.twig', $context );

get_footer();

?>