<?php
/*
Template Name: Notre Culture Rh
*/
?>
<?php
get_header();
$context                = Timber::get_context();
$context['racine']      = get_template_directory_uri();
$context['site']        = esc_url( home_url( '/' ) );

$context['posts']       = Timber::get_posts(array('pagename' => 'notre-culture-rh'));

if ( 'en' == pll_current_language() ) {
	$context['menu'] = new \Timber\Menu( 326 );
} else {
	$context['menu'] = new \Timber\Menu( 33 );
}

$context['fil']  = do_shortcode( '[wpseo_breadcrumb]' );

Timber::render('page-notreCultureRh.html.twig', $context);

get_footer();

?>