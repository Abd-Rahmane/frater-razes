<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package frater-razes
 */

?>
<footer class="footer p-5 color-3">
    <div class="container">
        <div class="row">
			<?php $logo_footer = get_field( 'logo_footer', 967 ); ?>
			<?php $logo_footer_en = get_field( 'logo_footer', 2028 ); ?>
	        <?php if ( pll_current_language() == 'en' ) { ?>
                <div class="col-sm-12 col-md-4"><img
                            src="<?php echo $logo_footer_en['url']; ?>"
                            class="logo-frater-footer mb-3 img-fluid"
                            alt="Frater"><?php the_field( 'text_footer_1', 2028 ); ?>
                </div>
	        <?php } else { ?>
            <div class="col-sm-12 col-md-4"><img
                        src="<?php echo $logo_footer['url']; ?>"
                        class="logo-frater-footer mb-3 img-fluid"
                        alt="Frater"><?php the_field( 'text_footer_1', 967 ); ?>
            </div>
	        <?php } ?>
            <div class="col-sm-6 col-md-4">
	            <?php if ( pll_current_language() == 'en' ) { ?>
                    <h4 class="color-2 mb-4"><?php _e( 'Browse', 'frater-razes' ) ?></h4>
	            <?php } else { ?>
                <h4 class="color-2 mb-4"><?php _e( 'Naviguer', 'frater-razes' ) ?></h4>
	            <?php } ?>
                <div>
					<?php
					wp_nav_menu( array(
						'theme_location' => 'menu-2',
						'menu_class'     => 'list-unstyled column-2 footer-nav'
					) );
					?>
                </div>
            </div>
            <div class="col-sm-6 col-md-4">
				<?php $social = get_field( 'reseaux_sociaux', 967 ); ?>
	            <?php if ( pll_current_language() == 'en' ) { ?>
                <h4 class="color-2 mb-4">Stay in touch!</h4>
                <p>Follow the FRATER-RAZES Laboratories on social media and get our latest news</p>
	            <?php } else { ?>
                    <h4 class="color-2 mb-4">Restez connectés</h4>
                    <p>Suivez Les Laboratoires Frater-Razes sur les réseaux sociaux et restez informés de nos nouvelles</p>
	            <?php } ?>
                <ul class="social-media-footer d-flex flex-row list-unstyled color-1">
                    <li class="list-inline-item rounded-circle">
                        <a href="<?php echo $social['facebook']; ?>"><i class="fab fa-facebook-f"></i></a>
                    </li>
                    <li class="list-inline-item rounded-circle"><a href="<?php echo $social['twitter']; ?>"><i
                                    class="fab fa-twitter"></i></a></li>
                    <li class="list-inline-item rounded-circle"><a href="<?php echo $social['linkedin']; ?>"><i
                                    class="fab fa-linkedin"></i></a></li>
                    <li class="list-inline-item rounded-circle"><a href="<?php echo $social['youtube']; ?>"><i
                                    class="fab fa-youtube"></i></a></li>
                </ul>
            </div>
        </div>
    </div>
</footer> <!-- #footer -->
<!-- footer -->
<div class="copyright blue-bg py-3 ">
    <div class="container">
        <div class="row text-center ">
            <div class="col-sm-4 text-left flex-center-center">
				<?php if ( pll_current_language() == 'en' ) { ?>
                    <p><span class="mr-3"><a href="<?php echo get_site_url(); ?>/en/legal-notices/">Legal Notices</a> </span>
                        <!--<span><a href="#">Plan de site</a></span>-->
                    </p>
				<?php } else { ?>
                    <p><span class="mr-3"><a href="<?php echo esc_url( home_url( '/' ) ); ?>mentions-legales/">Mentions légales</a> </span>
                        <!--<span><a href="#">Plan de site</a></span>-->
                    </p>
				<?php } ?>
            </div>
            <div class="col-sm-4 flex-center-center">
	            <?php if ( pll_current_language() == 'en' ) { ?>
                    <p>2019 © All rights reserved </p>
	            <?php } else { ?>
                    <p>2019 © Tous droits réservés. </p>
	            <?php } ?>
            </div>
            <div class="col-sm-4 text-right flex-center-center">
	            <?php if ( pll_current_language() == 'en' ) { ?>
                    <p>Designed and developed by <a href="https://shiftin.co/" target="_blank"><img
                                    src="<?php echo get_template_directory_uri(); ?>/views/img/logo-shiftin.png"
                                    class="w-25 pl-1"></a></p>
	            <?php } else { ?>
                    <p>Conçu et développé par <a href="https://shiftin.co/" target="_blank"><img
                                    src="<?php echo get_template_directory_uri(); ?>/views/img/logo-shiftin.png"
                                    class="w-25 pl-1"></a></p>
	            <?php } ?>
            </div>
        </div>
    </div>
</div> <!-- #footer -->

<?php wp_footer(); ?>

</body>
</html>
