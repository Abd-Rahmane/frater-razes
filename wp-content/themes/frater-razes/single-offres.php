<?php
get_header();

$offres = array(
	'post_type'      => 'offres',
	'posts_per_page' => -1,
	'order'          => 'ASC'
);

$context           = Timber::get_context();
$context['racine'] = get_template_directory_uri();
$context['site']   = esc_url( home_url( '/' ) );

$context['offres'] = new Timber\PostQuery( $offres );

$context['post'] = new Timber\Post();

$context['formulaire']  = do_shortcode( '[contact-form-7 id="238" title="Condidature"]' );

if ( 'en' == pll_current_language() ) {
	$context['formulaire']  = do_shortcode( '[contact-form-7 id="2320" title="Condidature EN"]' );
} else {
	$context['formulaire']  = do_shortcode( '[contact-form-7 id="238" title="Condidature"]' );
}

if ( 'en' == pll_current_language() ) {
	$context['menu'] = new \Timber\Menu( 326 );
} else {
	$context['menu'] = new \Timber\Menu( 33 );
}

$context['fil']  = do_shortcode( '[wpseo_breadcrumb]' );

Timber::render( 'page-detailPostePouvoir.html.twig', $context );

get_footer();

?>